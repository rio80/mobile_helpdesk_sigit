package com.apps.rio.sigithelpdesk.Model;

/**
 * Created by rio senjou on 09/12/2017.
 */

public class issue {
    String submit;
    String plan;
    String finish;
    String alldata;

    public String getIssueSubmit() {
        return submit;
    }

    public String getIssuePlan() {
        return plan;
    }

    public String getIssueFinish() {
        return finish;
    }

    public String getAlldata() {
        return alldata;
    }

}
