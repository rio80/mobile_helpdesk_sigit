package com.apps.rio.sigithelpdesk.Model;

/**
 * Created by rio senjou on 04/12/2017.
 */

public class userComplain {

    String no;
    String Nama;
    String Tanggal;
    String mesin;
    String section;
    String masalah;
    String Shift;
    String teknisi;
    String plan_Date;
    String Tanggal_selesai;
    String progress;
    String action;

    public userComplain(String no,
                        String nama,
                        String tanggal,
                        String mesin,
                        String section,
                        String masalah,
                        String shift,
                        String teknisi,
                        String plan_Date,
                        String tanggal_selesai,
                        String progress,
                        String action) {
        this.no = no;
        Nama = nama;
        Tanggal = tanggal;
        this.mesin = mesin;
        this.section = section;
        this.masalah = masalah;
        Shift = shift;
        this.teknisi = teknisi;
        this.plan_Date = plan_Date;
        Tanggal_selesai = tanggal_selesai;
        this.progress = progress;
        this.action = action;
    }
    public userComplain(String Nama,String Tanggal,String section,String Shift,String Tanggal_selesai,String progress){
        this.Nama=Nama;
        this.Tanggal=Tanggal;
        this.section=section;
        this.Shift=Shift;
        this.Tanggal_selesai=Tanggal_selesai;
        this.progress=progress;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public void setTanggal(String tanggal) {
        Tanggal = tanggal;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setShift(String shift) {
        Shift = shift;
    }

    public void setTanggal_selesai(String tanggal_selesai) {
        Tanggal_selesai = tanggal_selesai;
    }

    public String getNo() {
        return no;
    }

    public String getNama() {
        return Nama;
    }

    public String getTanggal() {
        return Tanggal;
    }

    public String getSection() {
        return section;
    }

    public String getShift() {
        return Shift;
    }

    public String getTanggal_selesai() {
        return Tanggal_selesai;
    }

    public String getMesin() {
        return mesin;
    }

    public String getMasalah() {
        return masalah;
    }

    public String getTeknisi() {
        return teknisi;
    }

    public String getPlan_Date() {
        return plan_Date;
    }

    public String getAction() {
        return action;
    }
}
