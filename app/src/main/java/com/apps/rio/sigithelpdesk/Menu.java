package com.apps.rio.sigithelpdesk;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.apps.rio.sigithelpdesk.Client.ApiClient;
import com.apps.rio.sigithelpdesk.Function.Alert;
import com.apps.rio.sigithelpdesk.Interface.ApiIssue;
import com.apps.rio.sigithelpdesk.Model.issue;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity implements View.OnClickListener {
    TextView issueSubmit, issuePlan, issueFinish, allData;
    ApiIssue apiIssue;
    SharedPreferences pref;
    private ProgressDialog loading;
    private Alert alert;
    private MainActivity ma;
    String getNama, getPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        alert = new Alert(Menu.this);
        pref = getSharedPreferences(ma.KEY_PREF, Context.MODE_PRIVATE);
        getSupportActionBar().setTitle("Menu DashBoard User");

        issueSubmit = (TextView) findViewById(R.id.issuesubmit);
        issuePlan = (TextView) findViewById(R.id.issueplan);
        issueFinish = (TextView) findViewById(R.id.issuefinish);
        allData = (TextView) findViewById(R.id.alldata);

        issueSubmit.setOnClickListener(this);
        issuePlan.setOnClickListener(this);
        issueFinish.setOnClickListener(this);
        allData.setOnClickListener(this);

        loading = new ProgressDialog(this);
        loading.setCancelable(false);
        loading.setMessage("Tunggu Sebentar..");
        loading.show();

        getNama = pref.getString(ma.KEY_USER, "");
        getRetrofit();

    }
    public void getRetrofit(){
        apiIssue = ApiClient.getClient().create(ApiIssue.class);
        Call<issue> callIssue = apiIssue.GetIssue(getNama);

        callIssue.enqueue(new Callback<issue>() {
            @Override
            public void onResponse(Call<issue> call, Response<issue> response) {

                issueSubmit.setText("Issue Submit : " + response.body().getIssueSubmit());
                issuePlan.setText("Issue Plan : " + response.body().getIssuePlan());
                issueFinish.setText("Issue Finish : " + response.body().getIssueFinish());
                allData.setText("All Data : " + response.body().getAlldata());
                loading.dismiss();
            }

            @Override
            public void onFailure(Call<issue> call, Throwable t) {
                alert.notif("Kesalahan!", "callIssue Error di : \n" + t.getMessage().toString());
            }
        });
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(Menu.this)
                .setTitle("LogOut")
                .setMessage("Anda Yakin Ingin Logout?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean(ma.KEY_REMEMBER, false);
                        editor.apply();

                        Menu.this.finish();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        dialog.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tambah:
                startActivity(new Intent(Menu.this, UserComplainInput.class));
                break;
            case R.id.refresh:
                getRetrofit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        ViewUserComplain user=new ViewUserComplain();
        if(v.getId()==R.id.issuesubmit){
            user.jenisView=user.getJenisView[0];
        }
        if(v.getId()==R.id.issueplan){
            user.jenisView=user.getJenisView[1];
        }
        if(v.getId()==R.id.issuefinish){
            user.jenisView=user.getJenisView[2];
        }
        if(v.getId()==R.id.alldata){
            user.jenisView="";
        }
        Intent i=new Intent(Menu.this,ViewUserComplain.class);
        i.putExtra("jenis_view",user.jenisView);
        startActivity(i);

    }
}
