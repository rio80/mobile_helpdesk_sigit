package com.apps.rio.sigithelpdesk.Interface;

import com.apps.rio.sigithelpdesk.Model.Value;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by rio senjou on 14/12/2017.
 */

public interface ApiMaster {

    @GET("dataBind.php")
    Call<Value> getMaster();
}
