package com.apps.rio.sigithelpdesk.Model;

/**
 * Created by rio senjou on 09/12/2017.
 */

public class pilihMenu {
    public static final String USER = "user";
    public static final String MAINTENANCE = "maintenance";
    public static String[] pilihan;
    public static String isiPilih;
    public static boolean aktif;
    public static boolean aktifPlan;
    public static boolean aktifFinish;
    public static String progres;

    public static String[] menu(){
       return pilih();
    }

    private static String[] pilih() {
        if (isiPilih.equals(USER)) {
            pilihan = new String[]{
                    "Lihat Data Claim user"
            };
        }
        if (isiPilih.equals(MAINTENANCE)) {
            pilihan = new String[]{
                    "Lihat Data Selesai",
                    "Lihat Data Pending",
                    "Lihat Semua Data"
            };
        }
        return pilihan;
    }
}

