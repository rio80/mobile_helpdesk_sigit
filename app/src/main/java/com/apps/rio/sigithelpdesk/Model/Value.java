package com.apps.rio.sigithelpdesk.Model;

import java.util.List;

/**
 * Created by rio senjou on 07/12/2017.
 */

public class Value {
    String value;
    String message;
    List<Login> result;
    List<userComplain> resultUser;
    List<master_mesin> result_mesin;
    List<master_plant> result_plant;
    List<master_teknisi> result_teknisi;

    public List<master_mesin> result_mesin() {
        return result_mesin;
    }

    public List<master_plant> result_plant() {
        return result_plant;
    }

    public List<master_teknisi> result_teknisi() {
        return result_teknisi;
    }

    public List<userComplain> getUserComplains() {
        return resultUser;
    }


    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public List<Login> getResult() {
        return result;
    }
}
