package com.apps.rio.sigithelpdesk;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.TextView;

import com.apps.rio.sigithelpdesk.Adapter.AdapterSubmit;
import com.apps.rio.sigithelpdesk.Client.ApiClient;
import com.apps.rio.sigithelpdesk.Function.Alert;
import com.apps.rio.sigithelpdesk.Interface.ApiIssue;
import com.apps.rio.sigithelpdesk.Model.Value;
import com.apps.rio.sigithelpdesk.Model.issue;
import com.apps.rio.sigithelpdesk.Model.pilihMenu;
import com.apps.rio.sigithelpdesk.Model.userComplain;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewUserComplain extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    AdapterSubmit adapterSubmit;
    List<userComplain> userComplains;
    ApiIssue apiIssue;
    SharedPreferences pref;

    ProgressDialog loading;
    String getUserName, jenisView, getJenisView[] = {
            "submit", "plan", "finish", "alldata"
    };
    private ViewUserComplain me = ViewUserComplain.this;
    private MainActivity ma = new MainActivity();
    private Alert alert;
    TextView userLogin, logOut;

    public boolean onOptionsItemSelected(MenuItem item) {
        //Pilihan Menu Toolbar
        switch (item.getItemId()) {
            case R.id.info:
                if (pilihMenu.isiPilih.equals(pilihMenu.USER)) {
                    startActivity(new Intent(me, UserComplainInput.class));
                }
                if (pilihMenu.isiPilih.equals(pilihMenu.MAINTENANCE)) {
                    munculPesanInfo();
                }

                break;
            case R.id.refresh:
                getDataFromRetrofit();
                break;
            default:
//                jenisView = "";
                break;
        }
        getDataFromRetrofit();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.item_view, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_user_complain);

        getSupportActionBar().setTitle("User Complain");
        alert = new Alert(me);
        userComplains = new ArrayList<userComplain>();

        recyclerView = (RecyclerView) findViewById(R.id.recycleUserComplain);
        userLogin = (TextView) findViewById(R.id.userLogin);
        logOut = (TextView) findViewById(R.id.Logout);

        pref = getSharedPreferences(ma.KEY_PREF, Context.MODE_PRIVATE);
        getUserName = pref.getString(ma.KEY_USER, "");

        if (pilihMenu.isiPilih.equals(pilihMenu.USER)) {
            Intent i = getIntent();
            jenisView = i.getStringExtra("jenis_view");
            logOut.setVisibility(View.INVISIBLE);
        }
        if (pilihMenu.isiPilih.equals(pilihMenu.MAINTENANCE)) {
            jenisView = "";
        }


        userLogin.setText("UserName : " + getUserName);
        loading = new ProgressDialog(this);
        loading.setMessage("Mohon Tunggu..");
        loading.setCancelable(false);
        loading.show();

        getDataFromRetrofit();

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(me)
                        .setTitle("LogOut")
                        .setMessage("Anda Yakin Ingin Logout?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                SharedPreferences.Editor editor = pref.edit();
                                editor.putBoolean(ma.KEY_REMEMBER, false);
                                editor.apply();

                                me.finish();
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                dialog.create().show();


            }
        });
    }

    public void getDataFromRetrofit() {
        Call<Value> callUser=null;
        apiIssue = ApiClient.getClient().create(ApiIssue.class);

        if (pilihMenu.isiPilih.equals(pilihMenu.USER)) {
            //jika level USER maka sorting query berdasarkan jenisView dan username
            callUser = apiIssue.GetUserComplain(jenisView, getUserName);
        }
        //jika level MAINTENANCE maka sorting query berdasarkan jenisView saja
        if (pilihMenu.isiPilih.equals(pilihMenu.MAINTENANCE)) {
            callUser = apiIssue.GetUserComplain(jenisView, "");
        }

        callUser.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {

                userComplains = response.body().getUserComplains();
                adapterSubmit = new AdapterSubmit(me, userComplains);
                layoutManager = new LinearLayoutManager(me);

                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapterSubmit);

                loading.dismiss();
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                loading.dismiss();
                alert.notif("Kesalahan!", "Error di : \n" + t.getMessage().toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (pilihMenu.isiPilih.equals(pilihMenu.USER)) {
            me.finish();
        }
        if (pilihMenu.isiPilih.equals(pilihMenu.MAINTENANCE)) {
            alert.pesan("LogOut", "Anda Yakin Ingin Logout?", false);
        }


    }

    public void munculPesanInfo() {
        final CharSequence[] charSequences = new CharSequence[4];
        apiIssue = ApiClient.getClient().create(ApiIssue.class);
        Call<issue> callIssue = apiIssue.GetIssue("");
        callIssue.enqueue(new Callback<issue>() {
            @Override
            public void onResponse(Call<issue> call, Response<issue> response) {
                charSequences[0] = ("All Data : " + response.body().getAlldata());
                charSequences[1] = ("Issue Submit : " + response.body().getIssueSubmit());
                charSequences[2] = ("Issue Plan : " + response.body().getIssuePlan());
                charSequences[3] = ("Issue Finish : " + response.body().getIssueFinish());

                AlertDialog.Builder dialog = new AlertDialog.Builder(ViewUserComplain.this);
                dialog.setTitle("Informasi");
                dialog.setItems(charSequences, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                jenisView = getJenisView[3];
                                break;
                            case 1:
                                jenisView = getJenisView[0];
                                break;
                            case 2:
                                jenisView = getJenisView[1];
                                break;
                            case 3:
                                jenisView = getJenisView[2];
                                break;
                        }
                        getDataFromRetrofit();
                    }
                });
                dialog.create().show();

            }

            @Override
            public void onFailure(Call<issue> call, Throwable t) {
                alert.notif("Error", "callIssue Error di : \n" + t.getMessage().toString());
            }
        });


    }
}
