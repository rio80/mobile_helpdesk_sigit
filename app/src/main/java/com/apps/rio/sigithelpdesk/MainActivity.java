package com.apps.rio.sigithelpdesk;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.apps.rio.sigithelpdesk.Client.ApiClient;
import com.apps.rio.sigithelpdesk.Function.Alert;
import com.apps.rio.sigithelpdesk.Interface.ApiLogin;
import com.apps.rio.sigithelpdesk.Model.Login;
import com.apps.rio.sigithelpdesk.Model.Value;
import com.apps.rio.sigithelpdesk.Model.pilihMenu;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText textUsername, textPassword;
    CheckBox chkRemember;
    Button btnLogin, btnExit;

    ApiLogin apiLogin;
    SharedPreferences pref;

    public final static String KEY_PREF = "keypref",
            KEY_USER = "keyuser",
            KEY_NRP = "keynrp",
            KEY_PASS = "keypass",
            KEY_REMEMBER = "keyremember";

    public Boolean cekRemember = false;
    private ProgressDialog progress;

    public List<Login> result = new ArrayList<>();

    public static String level, nrp;

    private Alert alert;

    public MainActivity ma = MainActivity.this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        alert = new Alert(ma);
        getSupportActionBar().setTitle("Login Aplikasi Helpdesk");
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");

        textUsername = (EditText) findViewById(R.id.editUserName);
        textPassword = (EditText) findViewById(R.id.editPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnExit = (Button) findViewById(R.id.btnExit);

        chkRemember = (CheckBox) findViewById(R.id.checkRemember);

        pref = getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);

        cekRemember = pref.getBoolean(KEY_REMEMBER, false);
        if (cekRemember) {
            textUsername.setText(pref.getString(KEY_USER, ""));
            textPassword.setText(pref.getString(KEY_PASS, ""));
            chkRemember.setChecked(true);
            masukLogin();
        } else {
            textUsername.setText("");
            textPassword.setText("");
            chkRemember.setChecked(false);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                masukLogin();
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.pesan("Exit", "Anda Yakin Ingin Keluar?",true);

            }
        });

    }


    @Override
    public void onBackPressed() {
        alert.pesan("Exit", "Anda Yakin Ingin Keluar?",true);
    }

    public void masukLogin(){
        progress.show();
        final String username = textUsername.getText().toString();
        final String password = textPassword.getText().toString();

        apiLogin = ApiClient.getClient().create(ApiLogin.class);

        Call<Value> call = apiLogin.login(username, password);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                //mengambil variabel value dan message dari JSON
                String value = response.body().getValue();
                String message = response.body().getMessage();

                //Jika UserName dan password benar maka Value berisi 1
                if (value.equals("1")) {
                    SharedPreferences.Editor editor = pref.edit();
                    if (chkRemember.isChecked()) editor.putBoolean(KEY_REMEMBER, true);
                    if (!chkRemember.isChecked()) editor.putBoolean(KEY_REMEMBER, false);

                    editor.putString(KEY_USER, username);
                    editor.putString(KEY_PASS, password);
                    editor.apply();

                    progress.dismiss();

                    Toast.makeText(ma,
                            message.toString(),
                            Toast.LENGTH_SHORT).show();

                    //Status Login Berhasil dan Lanjutkan cek auth level
                    //Simpan data NRP karyawan yang Login
                    apiLogin = ApiClient.getClient().create(ApiLogin.class);
                    Call<Value> callId = apiLogin.get_id(username, password);


                    callId.enqueue(new Callback<Value>() {
                        @Override
                        public void onResponse(Call<Value> call, Response<Value> response) {
                            result = response.body().getResult();
                            nrp = result.get(0).getNrp().toString();
                            level = result.get(0).getLevel().toString();
                            pilihMenu.isiPilih = level;


                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(MainActivity.KEY_NRP, nrp);
                            editor.apply();

                            if (pilihMenu.isiPilih.equals(pilihMenu.USER)) {
                                startActivity(new Intent(ma, Menu.class));

                            }
                            if (pilihMenu.isiPilih.equals(pilihMenu.MAINTENANCE)) {
                                startActivity(new Intent(ma, ViewUserComplain.class));
                            }
                            Toast.makeText(MainActivity.this,
                                    "Anda masuk Sebagai\n"+pilihMenu.isiPilih,
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Value> call, Throwable t) {
                            alert.notif("", t.getMessage().toString());
                        }
                    });


                    textPassword.setText(null);
                    textUsername.setText(null);
                } else {
                    progress.dismiss();
                    alert.notif(message, "Salah UserName atau Password");

                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progress.dismiss();
                alert.notif("", t.getMessage().toString());
            }
        });

    }
}
