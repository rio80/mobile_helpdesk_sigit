package com.apps.rio.sigithelpdesk;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.sigithelpdesk.Client.ApiClient;
import com.apps.rio.sigithelpdesk.Function.Alert;
import com.apps.rio.sigithelpdesk.Interface.ApiIssue;
import com.apps.rio.sigithelpdesk.Interface.ApiMaster;
import com.apps.rio.sigithelpdesk.Model.Value;
import com.apps.rio.sigithelpdesk.Model.master_mesin;
import com.apps.rio.sigithelpdesk.Model.master_plant;
import com.apps.rio.sigithelpdesk.Model.master_teknisi;
import com.apps.rio.sigithelpdesk.Model.pilihMenu;
import com.apps.rio.sigithelpdesk.Model.userComplain;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanResolveInput extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView judul, solveUserName, solvePlanDate;

    EditText solveMesin,
            solveLokasi,
            solveDeskripsi,
            solveShift;

    Spinner solveTeknisi;

    Button simpanSolve,
            batalSolve;

    PlanResolveInput me = PlanResolveInput.this;
    Alert alert;
    List<Value> result;
    List<master_teknisi> listTeknisi;
    ArrayList<String> bindTeknisi = new ArrayList<>();
    ApiIssue apiIssue;
    ApiMaster apiMaster;
    String tampungTeknisi;
    DatePickerDialog datePicker;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_resolve_input);

        init();

        apiIssue = ApiClient.getClient().create(ApiIssue.class);
        Call<userComplain> callApiIssue = apiIssue.viewAllData(id);
        callApiIssue.enqueue(new Callback<userComplain>() {
            @Override
            public void onResponse(Call<userComplain> call, Response<userComplain> response) {

                solveUserName.setText(response.body().getNama().toString());
                solveMesin.setText(response.body().getMesin().toString());
                solveLokasi.setText(response.body().getSection().toString());
                solveDeskripsi.setText(response.body().getMasalah().toString());
                solveShift.setText(response.body().getShift().toString());

            }

            @Override
            public void onFailure(Call<userComplain> call, Throwable t) {
                alert.notif("Error Callback Data", t.getMessage().toString());
            }
        });

        apiMaster = ApiClient.getClient().create(ApiMaster.class);
        Call<Value> callMaster = apiMaster.getMaster();
        callMaster.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                listTeknisi = response.body().result_teknisi();
                for (master_teknisi bind : listTeknisi) {
                    bindTeknisi.add(bind.getTeknisi().toString());
                }
                loadTeknisi();

            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                alert.notif("Error Callback Spinner", t.getMessage().toString());
            }
        });

        solvePlanDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.getTanggal(datePicker, solvePlanDate);
            }
        });

        simpanSolve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (solvePlanDate.equals("")) {
                    alert.notif("Pesan", "Pilih tanggal Planning date dulu!");
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(me)
                            .setTitle("Update Plan")
                            .setMessage("Yakin ingin Update Plan?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    simpan_plan();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    dialog.create().show();
                }
            }
        });

        batalSolve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                solvePlanDate.setText("");
                finish();
            }
        });
    }


    //untuk initialisasi Komponen di dalam OnCreate
    private void init() {
        getSupportActionBar().setTitle("Planning Maintenance");

        alert = new Alert(me);
        listTeknisi = new ArrayList<>();
        result = new ArrayList<>();
        Intent intent = getIntent();
        id = intent.getStringExtra("noticket");

        judul = (TextView) findViewById(R.id.judulResolve);
        solveUserName = (TextView) findViewById(R.id.solveUserName);
        solveMesin = (EditText) findViewById(R.id.solveMesin);
        solveLokasi = (EditText) findViewById(R.id.solveLokasi);
        solveDeskripsi = (EditText) findViewById(R.id.solveDeskripsi);
        solveShift = (EditText) findViewById(R.id.solveShift);
        solveTeknisi = (Spinner) findViewById(R.id.solveTeknisi);
        solvePlanDate = (TextView) findViewById(R.id.solvePlanDate);
        simpanSolve = (Button) findViewById(R.id.simpanSolve);
        batalSolve = (Button) findViewById(R.id.batalSolve);
        solvePlanDate.setText("");

        solveTeknisi.setOnItemSelectedListener(me);

        solveUserName.setEnabled(false);
        solveMesin.setEnabled(false);
        solveLokasi.setEnabled(false);
        solveDeskripsi.setEnabled(false);
        solveShift.setEnabled(false);

        judul.setText("Input Planning Maintenance");
    }



    private void simpan_plan() {
        String planning_date;
        planning_date = solvePlanDate.getText().toString();
        apiIssue = ApiClient.getClient().create(ApiIssue.class);
        Call<Value> getResult = apiIssue.submitToPlan(id, tampungTeknisi, planning_date);
        getResult.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {
                    alert.notif("Simpan Plan", "Berhasil Simpan Plan!");
                } else {
                    alert.notif("Simpan Plan", "Gagal Simpan Plan!");
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                alert.notif("Simpan Plan", t.getMessage().toString());
            }
        });
    }

    private void loadTeknisi() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(me, android.R.layout.simple_spinner_item, bindTeknisi);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        solveTeknisi.setAdapter(adapter);
    }

    //Procedure dari Implement AdapterView
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        tampungTeknisi = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
