package com.apps.rio.sigithelpdesk.Client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rio senjou on 07/12/2017.
 */

public class ApiClient {
    public static final String URL="http://192.168.58.1:81/SIGIT/coba/API/";
//public static final String URL="http://192.168.43.123:81/SIGIT/coba/API/";
    private static Retrofit retrofit=null;

   private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    public static Retrofit getClient(){
        if(retrofit==null){
            retrofit=new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}
