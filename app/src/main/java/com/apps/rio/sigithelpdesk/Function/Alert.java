package com.apps.rio.sigithelpdesk.Function;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;
import java.util.function.Function;

/**
 * Created by rio senjou on 12/12/2017.
 */

public class Alert {
    public boolean status=false;
    public String tampungTanggal;
    private Context context;

    private Context getContext() {
        return context;
    }

    public Alert(Context context) {
        this.context = context;
    }

    public void notif(String title, String message){

        AlertDialog.Builder pesan=new AlertDialog.Builder(getContext());
        pesan.setTitle(title);
        pesan.setMessage(message);
        pesan.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        pesan.create().show();
    }

    public void pesan(String title, String message, final boolean toClose) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(toClose==true){ System.exit(1);}
                        else {((Activity)context).finish();}
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        dialog.create().show();

    }

    public void getTanggal(DatePickerDialog datePicker, final TextView textView){
        final Calendar calendar = Calendar.getInstance();
        final int tanggal, bulan, tahun;

        //Untuk mendapatkan data tgl saat ini saat program berjalan
        tanggal = calendar.get(Calendar.DAY_OF_MONTH);
        bulan = calendar.get(Calendar.MONTH);
        tahun = calendar.get(Calendar.YEAR);

        //memunculkan dialog dateTime
        datePicker = new DatePickerDialog
                (context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        //Untuk menampung data tanggal yang dipilih dari DatePickerDialog
                        String tgl, tgl2;
                        tgl = String.valueOf(year) + "-" +
                                String.valueOf(month + 1) + "-" +
                                String.valueOf(dayOfMonth);

                        //Untuk membuat format tanggal dengan menampilkan nama Hari
                       /* SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = new Date(year,month,dayOfMonth);
                        tgl2 = simpleDateFormat.format(date);*/
                        textView.setText(tgl);
                    }
                    //Memberi settingan awal di datepickerdialog dengan tanggal saat program berjalan
                }, tahun, bulan, tanggal);
        datePicker.show();

    }


}
