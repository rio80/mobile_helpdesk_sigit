package com.apps.rio.sigithelpdesk;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.sigithelpdesk.Client.ApiClient;
import com.apps.rio.sigithelpdesk.Function.Alert;
import com.apps.rio.sigithelpdesk.Interface.ApiIssue;
import com.apps.rio.sigithelpdesk.Model.Value;
import com.apps.rio.sigithelpdesk.Model.userComplain;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinishInput extends AppCompatActivity {
    private TextView judul, finishUserName, finishDate2;

    private EditText finishMesin,
            finishLokasi,
            finishDeskripsi,
            finishShift,
            finishTeknisi,
            finishDate1,
            finishAction,
            finishPlanDate;


    private Button simpanFinish,
            batalFinish;

    private FinishInput me = FinishInput.this;
    private Alert alert;

    private ApiIssue apiIssue;
    private DatePickerDialog datePicker;
    private String finishDate, action,idt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_input);
        getSupportActionBar().setTitle("Finish Ticket");

        init();

        finishDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.getTanggal(datePicker, finishDate2);
            }
        });

        Intent intent = getIntent();
        idt = intent.getStringExtra("noticket");

        try {
            apiIssue = ApiClient.getClient().create(ApiIssue.class);
            Call<userComplain> callApiIssue = apiIssue.viewAllData(idt);
            callApiIssue.enqueue(new Callback<userComplain>() {
                @Override
                public void onResponse(Call<userComplain> call, Response<userComplain> response) {
                    finishUserName.setText(response.body().getNama().toString());
                    finishMesin.setText(response.body().getMesin().toString());
                    finishLokasi.setText(response.body().getSection().toString());
                    finishDeskripsi.setText(response.body().getMasalah().toString());
                    finishShift.setText(response.body().getShift().toString());
                    finishTeknisi.setText(response.body().getTeknisi().toString());
                    finishDate1.setText(response.body().getTanggal().toString());
                    finishPlanDate.setText(response.body().getPlan_Date().toString());

                    int progres;
                    progres=Integer.valueOf(response.body().getProgress());
                    if (progres==100){
                        finishDate2.setText(response.body().getTanggal_selesai().toString());
                        finishAction.setText(response.body().getAction().toString());
                    }else {
                        finishDate2.setText("");
                        finishAction.setText("");
                        finishDate2.setEnabled(true);
                        finishAction.setEnabled(true);

                    }
//                    Toast.makeText(me, "Berhasil getData", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<userComplain> call, Throwable t) {
                    alert.notif("Error Load Data", t.getMessage().toString());
                }
            });
        } catch (Exception e) {
            alert.notif("Error Load Data", e.getMessage().toString());
        }

        simpanFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finishDate = finishDate2.getText().toString();
                action = finishAction.getText().toString();

                if (finishDate.trim().equals("") || action.trim().equals("")) {
                    alert.notif("Finish Ticket", "Finish date atau Action belum di isi!");
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(me)
                            .setTitle("Update Plan")
                            .setMessage("Yakin ingin Finish Ticket?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                   simpanFinish();
                                   finish();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    dialog.create().show();
                }

            }
        });

        batalFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishDate2.setText("");
                finishAction.setText("");
                finish();
            }
        });
    }

    public void init() {

        alert = new Alert(me);

        judul = (TextView) findViewById(R.id.judulFinish);
        finishUserName = (TextView) findViewById(R.id.finishUserName);
        finishMesin = (EditText) findViewById(R.id.finishMesin);
        finishLokasi = (EditText) findViewById(R.id.finishLokasi);
        finishDeskripsi = (EditText) findViewById(R.id.finishDeskripsi);
        finishShift = (EditText) findViewById(R.id.finishShift);
        finishTeknisi = (EditText) findViewById(R.id.finishTeknisi);
        finishDate1 = (EditText) findViewById(R.id.finishTanggal);
        finishDate2 = (TextView) findViewById(R.id.finishDate);
        finishAction = (EditText) findViewById(R.id.finishAction);
        finishPlanDate = (EditText) findViewById(R.id.finishPlanDate);

        simpanFinish = (Button) findViewById(R.id.simpanFinish);
        batalFinish = (Button) findViewById(R.id.batalFinish);

        finishMesin.setEnabled(false);
        finishLokasi.setEnabled(false);
        finishDeskripsi.setEnabled(false);
        finishShift.setEnabled(false);
        finishTeknisi.setEnabled(false);
        finishDate1.setEnabled(false);
        finishPlanDate.setEnabled(false);
        finishDate2.setEnabled(false);
        finishAction.setEnabled(false);

        judul.setText("Input Planning Maintenance");

        finishDate2.setText("");
        finishAction.setText("");
    }

    public void simpanFinish() {
        apiIssue = ApiClient.getClient().create(ApiIssue.class);
        Call<Value> callValue = apiIssue.planToFinish(idt, finishDate, action);
        callValue.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                switch (value) {
                    //cek hasil value jika 1 maka data berhasil disimpan,
                    //jika 0 maka gagal simpan
                    case "1":
                        Toast.makeText(me, message, Toast.LENGTH_LONG).show();
                        break;
                    case "0":
                        Toast.makeText(me, message, Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                alert.notif("Gagal simpan",t.getMessage().toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
