package com.apps.rio.sigithelpdesk.Interface;

import com.apps.rio.sigithelpdesk.Model.Value;
import com.apps.rio.sigithelpdesk.Model.issue;
import com.apps.rio.sigithelpdesk.Model.userComplain;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by rio senjou on 09/12/2017.
 */

public interface ApiIssue {
    @FormUrlEncoded
    @POST("issue.php")
    Call<issue> GetIssue(@Field("user_name")String user_name);

    @FormUrlEncoded
    @POST("viewSubmit.php")
    Call<Value> GetUserComplain(@Field("jenis_view")String jenis_view,
                                @Field("user_name")String user_name);

    @FormUrlEncoded
    @POST("newTicket.php")
    Call<Value> SimpanTicket(@Field("nama_user")String nama_user,
                             @Field("mesin")String mesin,
                             @Field("section")String section,
                             @Field("masalah")String masalah,
                             @Field("shift")String shift);
    @FormUrlEncoded
    @POST("viewAllData.php")
    Call<userComplain> viewAllData(@Field("noticket")String noticket);

    @FormUrlEncoded
    @POST("submitToPlan.php")
    Call<Value> submitToPlan(@Field("no")String no,
                             @Field("teknisi")String teknisi,
                             @Field("plan_date")String plan_date);

    @FormUrlEncoded
    @POST("planToFinish.php")
    Call<Value> planToFinish(@Field("no")String no,
                             @Field("finish_date")String finish_date,
                             @Field("action")String action);
}
