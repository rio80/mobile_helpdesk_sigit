package com.apps.rio.sigithelpdesk.Model;

/**
 * Created by rio senjou on 04/12/2017.
 */

public class Login {
    private String userName;
    private String passWord;
    private String nrp;
    private String level;

    public Login(String userName, String passWord, String nrp, String level) {
        this.userName = userName;
        this.passWord = passWord;
        this.nrp = nrp;
        this.level = level;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
