package com.apps.rio.sigithelpdesk.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.rio.sigithelpdesk.FinishInput;
import com.apps.rio.sigithelpdesk.Menu;
import com.apps.rio.sigithelpdesk.Model.issue;
import com.apps.rio.sigithelpdesk.Model.pilihMenu;
import com.apps.rio.sigithelpdesk.Model.userComplain;
import com.apps.rio.sigithelpdesk.PlanResolveInput;
import com.apps.rio.sigithelpdesk.R;
import com.apps.rio.sigithelpdesk.ViewUserComplain;

import java.util.List;

/**
 * Created by rio senjou on 10/12/2017.
 */

public class AdapterSubmit extends RecyclerView.Adapter<AdapterSubmit.myViewHolder> {
    private Context context;
    private List<userComplain> userSubmit;
    pilihMenu menu;
    Intent intent;
    public AdapterSubmit(Context context, List<userComplain> userSubmit) {
        this.context = context;
        this.userSubmit = userSubmit;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_submit, parent, false);
        myViewHolder holder = new myViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        userComplain item = userSubmit.get(position);
        holder.No.setText(item.getNo());
        holder.Nama.setText(item.getNama());
        holder.Tanggal.setText(item.getTanggal());
        holder.section.setText(item.getSection());
        holder.shift.setText(item.getShift());
        holder.Tanggal_selesai.setText(item.getTanggal_selesai());


        if(item.getTanggal_selesai().equals("0000-00-00")){
            holder.Tanggal_selesai.setText("Ticket Belum Selesai");
            holder.Tanggal_selesai.setTextColor(context.getResources().getColor(R.color.progress0));
        }else {
            holder.Tanggal_selesai.setTextColor(context.getResources().getColor(R.color.progress100));
        }
        String progres;
        progres=item.getProgress();
        switch (progres) {
            case "100":
                holder.progress.setTextColor(context.getResources().getColor(R.color.progress100));
                break;
            case "50":
                holder.progress.setTextColor(context.getResources().getColor(R.color.progress50));
                break;
            case "0":
                holder.progress.setTextColor(context.getResources().getColor(R.color.progress0));
                break;
            default:
        }
        progres+=" %";
        holder.progress.setText(progres);

    }

    @Override
    public int getItemCount() {
        return userSubmit.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView No, Nama, Tanggal, section, shift, Tanggal_selesai, progress;

        public myViewHolder(View itemView) {
            super(itemView);
            No = (TextView) itemView.findViewById(R.id.idHelpdesk);
            Nama = (TextView) itemView.findViewById(R.id.namauser);
            Tanggal = (TextView) itemView.findViewById(R.id.tanggal);
            section = (TextView) itemView.findViewById(R.id.section);
            shift = (TextView) itemView.findViewById(R.id.shift);
            Tanggal_selesai = (TextView) itemView.findViewById(R.id.tanggalSelesai);
            progress = (TextView) itemView.findViewById(R.id.progress);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String getProgress;
                    String id;
                    int pos;
                    pos=getAdapterPosition();

                    userComplain item = userSubmit.get(pos);

                    getProgress=item.getProgress().toString();
                    id=item.getNo().toString();

                    switch (pilihMenu.isiPilih){
                        case pilihMenu.USER:
                            pilihMenu.aktif=false;

                            intent=new Intent(context.getApplicationContext(),FinishInput.class);

                            break;
                        case  pilihMenu.MAINTENANCE:
                            pilihMenu.aktif=true;

                            if(getProgress.equals("0")){
                                pilihMenu.aktifPlan=true;
                                pilihMenu.aktifFinish=true;
                                intent=new Intent(context.getApplicationContext(),PlanResolveInput.class);

                            }else if(getProgress.equals("50")){
                                pilihMenu.aktifPlan=false;
                                pilihMenu.aktifFinish=true;
                                intent=new Intent(context.getApplicationContext(),FinishInput.class);

                            }else if(getProgress.equals("100")){
                                pilihMenu.aktifPlan=false;
                                pilihMenu.aktifFinish=false;
                                intent=new Intent(context.getApplicationContext(),FinishInput.class);

                            }
                            break;
                    }
                    intent.putExtra("noticket",id.toString());
                    context.startActivity(intent);
                }
            });
        }
    }
}
