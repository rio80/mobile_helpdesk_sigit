package com.apps.rio.sigithelpdesk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.sigithelpdesk.Client.ApiClient;
import com.apps.rio.sigithelpdesk.Function.Alert;
import com.apps.rio.sigithelpdesk.Interface.ApiIssue;
import com.apps.rio.sigithelpdesk.Interface.ApiMaster;
import com.apps.rio.sigithelpdesk.Model.Value;
import com.apps.rio.sigithelpdesk.Model.master_mesin;
import com.apps.rio.sigithelpdesk.Model.master_plant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserComplainInput extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    ApiMaster apiMaster;
    ApiIssue apiIssue;

    List<master_mesin> result_mesin = new ArrayList<>();
    List<master_plant> result_plant = new ArrayList<>();

    ArrayList<String> bindMesin;
    ArrayList<String> bindLokasi;

    TextView nama;
    Spinner mesin, lokasi;
    EditText masalah, shift;
    Button btnSimpan, btnCancel;

    String tampungMesin, tampungLokasi;

    SharedPreferences pref;
    String nama_user, isi_mesin, section, problem, shif;
    Alert alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_complain_input);

        alert = new Alert(UserComplainInput.this);

        getSupportActionBar().setTitle("Input Ticket User");

        bindMesin = new ArrayList<>();
        bindLokasi = new ArrayList<>();

        nama = (TextView) findViewById(R.id.userName);
        mesin = (Spinner) findViewById(R.id.mesin);
        lokasi = (Spinner) findViewById(R.id.section);
        masalah = (EditText) findViewById(R.id.masalah);
        shift = (EditText) findViewById(R.id.shift);
        btnSimpan = (Button) findViewById(R.id.simpanticket);
        btnCancel = (Button) findViewById(R.id.batalticket);

        btnSimpan.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        pref = getSharedPreferences(MainActivity.KEY_PREF, Context.MODE_PRIVATE);

        nama.setText(pref.getString(MainActivity.KEY_USER, ""));

        try {
            apiMaster = ApiClient.getClient().create(ApiMaster.class);
            Call<Value> callMaster = apiMaster.getMaster();
            callMaster.enqueue(new Callback<Value>() {
                @Override
                public void onResponse(Call<Value> call, Response<Value> response) {
                    result_mesin = response.body().result_mesin();
                    result_plant = response.body().result_plant();

                    for (master_mesin binding : result_mesin) {
                        bindMesin.add(binding.getNama_mesin().toString());
                    }

                    for (master_plant binding : result_plant) {
                        bindLokasi.add(binding.getLokasi().toString());
                    }

                    loadLokasi();
                    loadMesin();
                }

                @Override
                public void onFailure(Call<Value> call, Throwable t) {
                    Toast.makeText(UserComplainInput.this,
                            "Error di\n" + t.getMessage().toString(),
                            Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
        }

        mesin.setOnItemSelectedListener(this);
        lokasi.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.simpanticket) {

            try {

                nama_user = nama.getText().toString();
                isi_mesin = tampungMesin;
                section = tampungLokasi;
                problem = masalah.getText().toString();
                shif = shift.getText().toString();

                if (problem.trim().equals("") || shif.trim().equals("")) {
                    alert.notif("Ticket Baru", "Masalah atau Shift harus di isi!");
                } else {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(UserComplainInput.this)
                            .setTitle("Ticket Baru")
                            .setMessage("Yakin ingin Buat Ticket Baru")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                simpanComplain();

                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    dialog.create().show();


                }

            } catch (Exception e) {
                alert.notif("Error di try", "Error\n" + e.getMessage().toString());
            }

        }
        if (v.getId() == R.id.batalticket) {
            finish();
        }
    }

    public void simpanComplain(){
        apiIssue = ApiClient.getClient().create(ApiIssue.class);
        Call<Value> simpanIssue = apiIssue.SimpanTicket(nama_user, isi_mesin, section, problem, shif);
        simpanIssue.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue().toString();
                String message = response.body().getMessage().toString();
                if (value.equals("1")) {
                    Toast.makeText(UserComplainInput.this, message, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    alert.notif("Ticket Baru", message);
                }

            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                alert.notif("Error Simpan", "Error simpan" + t.getMessage().toString());
            }
        });
    }
    public void loadMesin() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bindMesin);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mesin.setAdapter(adapter);

    }

    public void loadLokasi() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bindLokasi);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lokasi.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.mesin) {
            tampungMesin = parent.getItemAtPosition(position).toString();
        }
        if (parent.getId() == R.id.section) {
            tampungLokasi = parent.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
