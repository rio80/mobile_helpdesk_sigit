package com.apps.rio.sigithelpdesk.Interface;

import android.renderscript.Sampler;

import com.apps.rio.sigithelpdesk.Model.Login;
import com.apps.rio.sigithelpdesk.Model.Value;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by rio senjou on 07/12/2017.
 */

public interface ApiLogin {
    @FormUrlEncoded
    @POST("login.php")
    Call<Value> login(@Field("nama")String nama,
                      @Field("password")String password);

    @FormUrlEncoded
    @POST("login_id.php")
    Call<Value> get_id(@Field("nama")String nama,
                       @Field("password")String password);

}
